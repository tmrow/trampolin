<?php include 'inc/header.php'; ?>

<?php if($obra): ?>

    <!-- VISIBLE XS -->
    <?php
        $active='home';
        include 'inc/xs/navbar-segments.php';
    ?>

<?php endif; ?>
<?php if($obra): ?>
    <?php if($gira_activa): ?>
<div class="container" id="content">
        <?php include 'inc/sidebar.php'; ?>
        <div id="primary">
                <!-- VISIBLE LG -->
                <div class="row tabs-menu d-none d-lg-block" id="alerts-menu">
                    <h1 class="main-title">Alertas</h1>
                    <ul class="nav nav-tabs" id="alerts-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="all-tab" href="#">Todas (5)</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="sistem-tab" href="#">Sistema (2)</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="order-tab" href="#">Pedidos (1)</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="confirmations-tab" href="#">Confirmaciones (2)</a>
                        </li>
                    </ul>
                </div>

                <!-- VISIBLE XS -->
                <div class="select-tabs d-block d-lg-none py-2">
                    <select class="form-control filter">
                        <option id="all-option">
                            Todas
                        </option>
                        <option id="system-option">
                            Sistema
                        </option>
                        <option id="order-option">
                            Pedidos
                        </option>
                        <option id="all-option">
                            Confirmaciones
                        </option>
                    </select>
                </div>

                <div class="card-container" id="alerts-body">
                    <?php include "inc/alerts/alert-order.php"; ?>
                    <?php include "inc/alerts/alert-system.php"; ?>
                    <?php include "inc/alerts/alert-confirmation.php"; ?>
                </div>

        </div>
</div>
    <?php else: ?>

        <?php include 'inc/xs/navbar-segments.php'; ?>

        <div class="container open-modal perfil" id="content">
            <?php include 'inc/sidebar.php'; ?>
            <div id="primary">
                <div class="break-container form-header">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="f orm-title">
                                <h1 class="main-title">Activar la gira</h1>
                                <div class="d-flex w-100 align-items-center">
                                    <div class="c100 small p34 mr-4">
                                        <span>34%</span>
                                        <div class="slice">
                                            <div class="bar"></div>
                                            <div class="fill"></div>
                                        </div>
                                    </div>
                                    <p>
                                        <span class="text-danger lead"><strong>No estás emitiendo alertas</strong></span><br>
                                        Para poder activar tu gira debes completar tu perfil
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="list-group mt-3">
                    <div class="list-group-item">
                        <div class="d-flex w-100 justify-content-between align-items-center">
                            <p><i class="far fa-check-circle text-success mr-1"></i> Ficha del espectáculo</p>
                            <a href="ficha-espectaculo.php" class="btn-link small">EDITAR</a>
                        </div>
                    </div>
                    <div class="list-group-item">
                        <div class="d-flex w-100 justify-content-between align-items-center">
                            <p><i class="far fa-check-circle text-success mr-1"></i> Cachet</p>
                            <a href="ficha-espectaculo-cachet.php" class="btn-link small">EDITAR</a>
                        </div>
                    </div>
                    <div class="list-group-item">
                        <div class="d-flex w-100 justify-content-between align-items-center">
                            <p><i class="far fa-times-circle text-danger mr-1"></i> Multimedia</p>
                            <a href="ficha-espectaculo-multimedia.php" class="btn-link small">EDITAR</a>
                        </div>
                    </div>
                    <div class="list-group-item">
                        <div class="d-flex w-100 justify-content-between align-items-center">
                            <p><i class="far fa-times-circle text-danger mr-1"></i> Agenda</p>
                            <a href="ficha-espectaculo-multimedia.php" class="btn-link small">EDITAR</a>
                        </div>
                    </div>
                    <div class="list-group-item">
                        <div class="d-flex w-100 justify-content-between align-items-center">
                            <p><i class="far fa-check-circle text-success mr-1"></i> Requerimientos técnicos</p>
                            <a href="ficha-espectaculo-multimedia.php" class="btn-link small">EDITAR</a>
                        </div>
                    </div>
                    <div class="list-group-item">
                        <div class="d-flex w-100 justify-content-between align-items-center">
                            <p><i class="far fa-check-circle text-success mr-1"></i> Requisitos a cubrir</p>
                            <a href="ficha-espectaculo-multimedia.php" class="btn-link small">EDITAR</a>
                        </div>
                    </div>
                    <div class="spacer-2"></div>
                </div>
            </div>
        </div>

    <?php endif; ?>

<?php else: ?>

    <?php include 'inc/slides.php'; ?>

<?php endif; ?>
<?php include 'inc/footer.php'; ?>
