<?php include 'inc/header.php'; ?>
<?php include 'inc/xs/navbar-nav.php'; ?>
<div class="container open-modal form" id="content">
    <?php include 'inc/sidebar.php'; ?>
    <a href="ficha-espectaculo.php" class="back-button">
        <i class="fas fa-arrow-left pr-1"></i> Volver
    </a>
    <div id="primary" class="form">
        <div class="break-container form-header">
            <div class="row">
                <div class="col-lg-8">
                    <div class="form-title">
                        <h1>Ingresa tu cachet</h1>
                        <p class="lead">Dinos cuando y donde tienes programado estar para generar tu circuito</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="list- group">
                    <form action="">
                        <div class="form-group">
                            <label>Cachet nacional</label>
                            <input type="number" id="cachet-nacional" name="cachet-nacional" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Cachet internacional</label>
                            <input type="number" id="cachet-internacional" name="cachet-internacional" class="form-control">
                        </div>
                    </form>
                </div>
            </div>
            <div class="spacer-2"></div>
        </div>
        <div class="spacer-2 d-block d-lg-none"></div>
        <a href="ficha-espectaculo-multimedia.php" class="btn btn-primary btn-block-xs-only float-lg-left">CONTINUAR</a>
    </div>
</div>
<?php include 'inc/footer.php'; ?>
