
<div class="card confirmation alerts w-100" data-link="propuesta-1.php">
    <div class="card-body">
        <div class="row align-items-center">
            <div class="col-12 col-lg-9">
                <p><i class="fas fa-check pr-2"></i><small>Hace dos horas</small></p>
                <h5 class="py-1">Teatro Colón confirmó tu participación</h5>
            </div>
            <div class="col-3 d-none d-lg-block text-right">
                <a href="#" class="btn btn-link btn-sm">VER LA PROPUESTA</a>
            </div>
        </div>
    </div>
</div>