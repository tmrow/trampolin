<div class="card system alerts w-100">
    <div class="card-body">
        <div class="row align-items-center">
            <div class="col-12 col-lg-9">
                <p><i class="fas fa-cog pr-2"></i><small>Hace dos horas</small></p>
                <h5 class="py-1">Tenemos nuevas funciones</h5>
            </div>
            <div class="col-3 d-none d-lg-block text-right">
                <a href="#" class="btn btn-link btn-sm">VER</a>
            </div>
        </div>
    </div>
</div>