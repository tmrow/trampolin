<div class="card order alerts w-100 clickeable" data-link="propuesta-1.php">
    <div class="card-body">
        <div class="row align-items-center">

            <div class="col-12 col-lg-9">
                <p><i class="fas fa-bullseye pr-2"></i><small>Hace dos horas</small></p>
                <h5 class="py-1">¡El Teatro Colón eligió tu obra!</h5>
            </div>
            <div class="col-3 d-none d-lg-block text-right">
                <a href="#" class="btn btn-link btn-sm">VER LA PROPUESTA</a>
            </div>
        </div>
    </div>
</div>