<div class="absolute home-slides">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="icon"></div>
                        <h1>¡Hola Marcos!</h1>
                        <h3>Bienvenido a Trampolín</h3>
                        <p>Somos una plataforma que optimiza la presentación de espectáculos por  proximidad geográfica</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>