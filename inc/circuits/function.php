<div class="card order pending w-100 clickeable" data-link="propuesta-1.php">
    <div class="card-body">
        <div class="row align-items-center">
            <div class="col-12 col-lg-9 align-self-center top-info">
                <div class="date text-center pr-2 float-left">
                    <span class="lead">12</span><br>
                    <span class="small">AGO</span>
                </div>
                <div class="title">
                    <h5>Teatro Colón</h5>
                    <p class="small">Mar del Plata, Argentina</p>
                </div>
            </div>
            <div class="col-3 d-none d-lg-block text-right">
                <a href="#" class="btn btn-link btn-sm">VER LA PROPUESTA</a>
            </div>
        </div>
    </div>
</div>