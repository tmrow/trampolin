<div class="card order pending w-100 clickeable" data-link="propuesta-1.php">
        <div class="card-body">
            <div class="row align-items-center">
                <div class="col-12 col-lg-4 align-self-center top-info">
                    <div class="row">
                        <div class="col-2">
                            <div class="image">
                                <img src=" http://via.placeholder.com/50x50" width="50" height="50" alt="Teatro Colón">
                            </div>
                        </div>
                        <div class="col-10">
                            <div class="title">
                                <h5>Teatro Colón</h5>
                                <p class="small">Mar del Plata, Argentina</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 bottom-info">
                    <div class="row">
                        <div class="col-6 col-lg-4">
                            <span class="small">Cachet propuesto</span>
                            <div class="value">$12.000</div>
                        </div>
                        <div class="col-6 col-lg-4">
                            <span class="small">Fecha estimada</span>
                            <div class="value">5/5/2018</div>
                        </div>
                        <div class="col-4 d-none d-lg-block">
                            <span class="small">Estado</span>
                            <div class="value text-warning">Pendiente</div>
                        </div>
                    </div>
                </div>
                <div class="col-3 d-none d-lg-block text-right">
                    <a href="propuesta-1.php" class="btn btn-link btn-sm">VER LA PROPUESTA</a>
                </div>
            </div>
        </div>
    </div>