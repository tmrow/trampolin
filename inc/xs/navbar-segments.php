<nav class="navbar xs d-block d-lg-none pb-0">
    <div class="row">
        <div class="col">
            <div class="bars-menu">
                <a href="#">
                    <i class="fas fa-bars text-primary"></i>
                </a>
            </div>
        </div>
        <div class="col">
            <a href="home.php" class="navbrar-brand">
                <img src="assets/img/logo-trampolin.svg">
            </a>
        </div>
        <div class="col">
            <?php if($obra): ?>
                <div class="dropdown float-right">
                    <a class="dropdown-toggle" id="dropdown-obras" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="http://via.placeholder.com/30x30">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <span class="dropdown-item" href="#"><strong>Cambiar de obra</strong></span>

                        <a class="dropdown-item" href="#"><img src="http://via.placeholder.com/30x30" class="mr-2"> Mi otra obra</a>
                        <a class="dropdown-item" href="#"><img src="http://via.placeholder.com/30x30" class="mr-2"> Mi otra obra</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#"><i class="fas fa-plus-circle"></i> Agregar nueva obra</a>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <?php if($obra): ?>
        <?php include 'inc/xs/segments.php'; ?>
    <?php endif; ?>
</nav>