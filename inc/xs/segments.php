<!-- Nav tabs -->
<div class="segments-cont mt-3">
    <ul class="nav nav-tabs" id="segments" role="tablist">
        <li class="nav-item">
            <a class="nav-link <?php if ($active=='home'){echo 'active';} ?>" href="home.php"><i class="far fa-bell d-inline"></i> Alertas</a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?php if ($active=='propuestas'){echo 'active';} ?>"  href="propuestas.php"><i class="fas fa-inbox d-inline"></i> Propuestas</a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?php if ($active=='circuito'){echo 'active';} ?>"  href="circuito.php"><i class="fas fa-inbox d-inline"></i> Circuito</a>
        </li>
    </ul>
</div>