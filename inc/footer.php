<div class="spacer-2"></div>
</div> <!-- main-content -->
<!-- build:js -->
<script src="../assets/js/main.js"></script>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            message: 'Hello Vue!'
        }
    })
</script>

<!-- endbuild -->
</body>

</html>