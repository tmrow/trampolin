<div class="header-obra d-none d-lg-block" id="sidebar">
    <img src="assets/img/logo-trampolin.svg" class="py-4" alt="Logo Trampolín">
    <div class="image full-rounded mb-2">
        <img src="http://via.placeholder.com/60x60" class="rounded" alt="">
    </div>
    <h5>Título de la obra</h5>
    <span class="small text-primary"><i class="fas fa-check-circle"></i> Emitiendo alertas</span>
    <span class="small text-danger d-none"><i class="fas fa-times"></i> Gira cerrada</span>
    <ul class="nav flex-column pt-3 pl-0 ml-0">
        <li class="nav-item">
            <a class="nav-link active" href="home.php"><i class="far fa-bell"></i> Alertas</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="propuestas.php"><i class="fas fa-inbox"></i> Propuestas</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="circuito.php"><i class="fas fa-map-marker-alt"></i> Circuito</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#"><i class="fas fa-eye"></i> Perfil de la obra</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#"><i class="fas fa-cog"></i> Ajustes de gira</a>
        </li>
        <li class="nav-item pr-5">
           <hr/>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#"><i class="fas fa-credit-card"></i> Pagos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#"><i class="far fa-user-circle-o"></i> Mi cuenta</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#"><i class="fas fa-info-circle"></i> Ayuda</a>
        </li>
    </ul>
</div>