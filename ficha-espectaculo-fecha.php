<?php include 'inc/header.php'; ?>
<?php include 'inc/xs/navbar-nav.php'; ?>
<div class="container open-modal form" id="content">
    <?php include 'inc/sidebar.php'; ?>
    <a href="ficha-espectaculo-cachet.php" class="back-button">
        <i class="fas fa-arrow-left pr-1"></i> Volver
    </a>
    <div id="primary" class="form">
        <div class="break-container form-header">
            <div class="row">
                <div class="col-lg-8">
                    <div class="form-title">
                        <h1>Añadir tu fecha programada</h1>
                        <p class="lead">Dinos cuando y donde tienes programado estar para generar tu circuito</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="list- group">
                    <form action="">
                        <div class="form-group">
                            <label>Personas en gira</label>
                            <input type="number" id="personas-gira" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Horas de montaje y desmontaje</label>
                            <input type="number" id="montaje" name="montaje" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Espacio escénico</label>
                            <input type="text" id="espacio-escenico" name="espacio-escenico" class="form-control" placeholder="Ancho x profundidad x altura">
                        </div>
                        <div class="form-group">
                            <label>Sonido</label>
                            <input type="text" id="sonido" name="sonido" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Escenografía</label>
                            <textarea type="text" id="escenografia" name="escenografia" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Iluminación</label>
                            <input type="text" id="iluminacion" name="iluminacion" class="form-control">
                        </div>
                    </form>
                </div>
            </div>
            <div class="spacer-2"></div>
        </div>
        <div class="spacer-2 d-block d-lg-none"></div>
        <a href="ficha-espectaculo-cachet.php" class="btn btn-primary btn-block-xs-only float-lg-left">CONTINUAR</a>
    </div>
</div>
<?php include 'inc/footer.php'; ?>
