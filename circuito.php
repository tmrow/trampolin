<?php include 'inc/header.php'; ?>
<?php if($obra): ?>

    <!-- VISIBLE XS -->
    <?php
    $active='circuito';
    include 'inc/xs/navbar-segments.php';
    ?>

<?php endif; ?>

<div class="container" id="content">
<?php include 'inc/sidebar.php'; ?>
    <div id="primary">
        <?php if($obra): ?>
            <!-- VISIBLE LG -->
            <div class="row tabs-menu d-none d-lg-block" id="circuits-menu">
                <h1 class="main-title">Circuito</h1>
                <ul class="nav nav-tabs" id="alerts-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="all-tab" href="#">Histórico</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="2018-tab" href="#">2018</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="2017-tab" href="#">2017</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="2016-tab" href="#">2016</a>
                    </li>
                </ul>
            </div>

            <!-- VISIBLE XS -->
            <div class="select-tabs d-block d-lg-none py-2">
                <select class="form-control filter">
                    <option id="all-option">
                        Histórico
                    </option>
                    <option id="2018-option">
                        2018
                    </option>
                    <option id="2017-option">
                        2017
                    </option>
                    <option id="2016-option">
                        2016
                    </option>
                </select>
            </div>

            <div class="card-container" id="circuit-body">
                <?php include "inc/circuits/function.php"; ?>
            </div>

        <?php else: ?>

            <?php include 'inc/xs/slides.php'; ?>

        <?php endif; ?>

    </div>
</div>
<?php include 'inc/footer.php'; ?>
