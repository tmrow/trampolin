<?php include 'inc/header.php'; ?>
<?php include 'inc/xs/navbar-nav.php'; ?>

<div class="container open-modal" id="content">
    <?php include 'inc/sidebar.php'; ?>
    <a href="propuestas.php" class="back-button"><i class="fas fa-arrow-left"></i> Volver</a>
    <div id="primary">
            <div class="row tabs-menu d-none d-lg-block" id="circuits-menu">
                <h1 class="main-title">Propuesta de participación</h1>
                <div class=""></div>
            </div>
            <div class="spacer-2 d-none d-lg-block"></div>
            <div class="list-group">
                <div class="list-group-item">
                    <div class="d-flex w-100 justify-content-between align-items-center clickeable" data-link="teatro-1.php">
                        <img src="http://via.placeholder.com/50x50" class="full-rounded float-left mb-3 mr-4" width="50" height="50" alt="Teatro Municipal Colón">
                        <h4 class="mr-auto">Teatro Municipal Colón</h4>
                        <a href="#"><span class="d-none d-lg-inline-block"> VER PERFIL</span> <i class="fas pl-1 fa-arrow-right"></i></a>
                    </div>
                </div>
                <div class="list-group-item">
                    <div class="d-flex w-100 justify-con tent-between align-items-center">
                        <div>
                            <h5>Ubicación</h5>
                            <p class="align-self-auto">Hipolito Irigoyen 2450, Mar del Plata Argentina</p>
                        </div>
                        <a href="#"><span class="d-none d-lg-inline-block"> VER EN MAPA</span> <i class="fas pl-1 fa-arrow-right"></i></a>
                    </div>
                </div>
                <div class="list-group-item">
                    <h5>Fecha propuesta</h5>
                    <p>05 de agosto de 2017, 19:30 hs</p>
                </div>
                <div class="list-group-item">
                    <h5>Mensaje</h5>
                    <p>Nos encantaría que su obra esté en nuestra sala. Podemos charlar las fechas y los días.</p>
                </div>
                <div class="list-group-item">
                    <h5>Cachet propuesto</h5>
                    <p>$12.000</p>
                </div>
                <div class="list-group-item">
                    <div class="d-flex w-100 justify-content-between align-items-center clickeable">
                        <div>
                            <h5>Sala</h5>
                            <p>Sala Nicanor Otamendi </p>
                        </div>
                        <a href="#"><span class="d-none d-lg-inline-block"> VER EN MAPA</span> <i class="fas pl-1 fa-arrow-right"></i></a>
                    </div>
                </div>
                <div class="spacer-2"></div>
            </div>
            <a href="#" class="btn btn-primary btn-block-xs-only float-lg-left">ACEPTAR PROPUESTA</a>
            <div class="spacer-2 d-block d-lg-none"></div>
            <a href="#" class="btn btn-link mr-auto btn-block-xs-only float-lg-left mb-4">SOLICITAR UN CAMBIO</a>
            <a href="#" class="btn btn-outline-danger btn-block-xs-only float-lg-right mb-4">RECHAZAR</a>
    </div>
</div>
<?php include 'inc/footer.php'; ?>
