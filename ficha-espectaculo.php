<?php include 'inc/header.php'; ?>
<?php include 'inc/xs/navbar-nav.php'; ?>
<div class="container open-modal form" id="content">
    <?php include 'inc/sidebar.php'; ?>
    <a href="home.php" class="back-button">
        <i class="fas fa-arrow-left pr-1"></i> Volver
    </a>
    <div id="primary" class="form">
        <div class="break-container form-header">
            <div class="row">
                <div class="col-lg-8">
                    <div class="form-title">
                        <h1>Completa los datos</h1>
                        <p class="lead">Dinos cuando y donde tienes programado estar para generar tu circuito</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="list- group">
                    <form action="">
                        <div class="form-group">
                            <label>Nombre del espectáculo</label>
                            <input type="text" id="nombre" name="nombre" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Autor</label>
                            <input type="text" id="autor" name="autor" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Director</label>
                            <input type="text" id="director" name="director" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Actores</label>
                            <input type="text" id="actores" name="actores" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Síntesis argumental</label>
                            <textarea id="sintesis" name="sintesis" type="text" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Ciudad de origen</label>
                            <input type="text" id="ciudad" name="ciudad" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Fecha de estreno</label>
                            <input id="datepicker" id="fecha" name="fecha" type="text" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label>Género</label>
                            <select class="form-control" id="genero" name="genero">
                                <option>Comedia</option>
                                <option>Musical</option>
                                <option>Drama</option>
                                <option>Infantil</option>
                                <option>Òpera</option>
                                <option>Danza</option>
                                <option>Stand-Up</option>
                                <option>Clown</option>
                                <option>De objetos</option>
                                <option>Otro</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
            <div class="spacer-2"></div>
        </div>
        <div class="spacer-2 d-block d-lg-none"></div>
        <a href="ficha-espectaculo-cachet.php" class="btn btn-primary btn-block-xs-only float-lg-left">CONTINUAR</a>
    </div>
</div>
<?php include 'inc/footer.php'; ?>
