<?php include 'inc/header.php'; ?>
<div class="container-fluid">
    <div class="spacer-2"></div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-5" id="login-form">
                <div class="text-center">
                    <img src="assets/img/logo-trampolin.svg" class="mx-auto" alt="Logo Trampolín" width="120">
                </div>
                <div class="spacer-1"></div>
                <h4 class="text-center" id="app">¿Listo para saltar?</h4>
                <div class="spacer-1"></div>
                <a href="#" class="btn btn-info btn-block btn-lg-inline">INGRESAR CON FACEBOOK</a>
                <div class="spacer-2"></div>
                <div class="lined relative">
                    <div class="line w-100"></div>
                    <div class="text-center mb-4"><small>O COMPLETA TUS DATOS</small></div>
                </div>
                <form action="">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Ingresa tu email">
                        <label>Email</label>
                    </div>
                    <div class="form-group password">
                        <input type="password" class="form-control" placeholder="Contaseña">
                        <a href="recordar-contrasena.php" class="recover-pass">Recordar contraseña</a>
                        <label>Contraseña</label>
                    </div>
                    <div class="form-group">
                        <a href="home.php" class="btn btn-primary btn-block">INICIA SESIÓN</a>
                    </div>
                </form>
                <div class="text-center">
                    <a href="crear-cuenta.php" class="btn btn-link mx-auto">Registrate ahora</a>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="bg-light"></div>
            </div>
        </div>
    </div>
</div>
<?php include 'inc/footer.php'; ?>
