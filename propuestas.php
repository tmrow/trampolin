<?php include 'inc/header.php'; ?>

<?php
    if($obra):
    /*
     * Si hay una obra previamente cargada puedo mostrar el contenido. En caso contrario,
     * se abre el formulario para cargarla. No podemos mostrar info si no hay obra.
     */
    ?>
    <?php
    $active='propuestas';
    include 'inc/xs/navbar-segments.php';
    ?>
<?php endif; ?>
<div class="container" id="content">
    <?php include 'inc/sidebar.php'; ?>
    <div id="primary">
        <?php if($obra): ?>
            <!-- VISIBLE LG -->
            <div class="row tabs-menu d-none d-lg-block" id="alerts-menu">
                <h1 class="main-title">Propuestas</h1>
                <ul class="nav nav-tabs" id="alerts-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="all-tab" href="#">Todas (5)</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pending-tab" href="#">Pendientes (2)</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="acepted-tab" href="#">Aceptadas (1)</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="rejected-tab" href="#">Rechazadas (2)</a>
                    </li>
                </ul>
            </div>

            <!-- VISIBLE XS -->
            <div class="select-tabs d-block d-lg-none py-2">
                <select class="form-control filter">
                    <option id="all-option">
                        Todas
                    </option>
                    <option id="pending-option">
                        Pendientes
                    </option>
                    <option id="acepted-option">
                        Aceptadas
                    </option>
                    <option id="rejected-option">
                        Rechazadas
                    </option>
                </select>
            </div>

            <div class="card-container" id="orders-body">
                <?php include "inc/orders/order-pending.php"; ?>
                <?php include "inc/orders/order-pending.php"; ?>
                <?php include "inc/orders/order-pending.php"; ?>
            </div>

        <?php else: ?>

            <?php include 'inc/xs/slides.php'; ?>

        <?php endif; ?>

    </div>
</div>
<?php include 'inc/footer.php'; ?>
