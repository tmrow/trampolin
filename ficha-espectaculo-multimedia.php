<?php include 'inc/header.php'; ?>
<?php include 'inc/xs/navbar-nav.php'; ?>
<div class="container open-modal form" id="content">
    <?php include 'inc/sidebar.php'; ?>
    <a href="ficha-espectaculo-cachet.php" class="back-button">
        <i class="fas fa-arrow-left pr-1"></i> Volver
    </a>
    <div id="primary" class="form">
        <div class="break-container form-header">
            <div class="row">
                <div class="col-lg-8">
                    <div class="form-title">
                        <h1>Multimedia</h1>
                        <p class="lead">Dinos cuando y donde tienes programado estar para generar tu circuito</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="list- group">
                    <form action="">
                        <div class="form-group">
                            <label>Imagen de perfil</label>
                            <input type="file" class="form-control pt-2" id="" aria-describedby="">
                        </div>
                        <div class="form-group">
                            <label>Galería</label>
                            <div class="gallery bg-light" style="height: 300px;">
                           </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="spacer-2"></div>
        </div>
        <div class="spacer-2 d-block d-lg-none"></div>
        <a href="ficha-espectaculo-tecnico.php" class="btn btn-primary btn-block-xs-only float-lg-left">CONTINUAR</a>
    </div>
</div>
<?php include 'inc/footer.php'; ?>
